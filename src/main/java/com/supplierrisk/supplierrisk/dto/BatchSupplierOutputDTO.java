package com.supplierrisk.supplierrisk.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author joydeep.chakraborty
 *
 */
public class BatchSupplierOutputDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long batchId;
	private Map<String,SupplierSearchOutputDTO> mapOFSupplierTODunsData;
	
	public Map<String, SupplierSearchOutputDTO> getMapOFSupplierTODunsData() {
		return mapOFSupplierTODunsData;
	}
	public void setMapOFSupplierTODunsData(Map<String, SupplierSearchOutputDTO> mapOFSupplierTODunsData) {
		this.mapOFSupplierTODunsData = mapOFSupplierTODunsData;
	}
	public long getBatchId() {
		return batchId;
	}
	public void setBatchId(long batchId) {
		this.batchId = batchId;
	}

	
	
	
	
}

