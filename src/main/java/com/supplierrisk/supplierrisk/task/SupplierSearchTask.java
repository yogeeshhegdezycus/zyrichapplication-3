package com.supplierrisk.supplierrisk.task;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.supplierrisk.supplierrisk.controller.SupplierRiskZyrichController;
import com.supplierrisk.supplierrisk.dto.LocationDTO;
import com.supplierrisk.supplierrisk.dto.SupplierSearchOutputDTO;
import com.supplierrisk.supplierrisk.dto.TenantSearchDTO;
import com.zycus.zyrich.utils.ISOAlphaNumericGenerator;
import com.zycus.zyrich.utils.SpecialCharacterCleansing;
import com.zycus.zyrichservice.invoked.InvokeCleaning;
import com.zycus.zyrichservice.invoked.ZyrichInvoked;

public class SupplierSearchTask implements Runnable {
	
	Map<String, SupplierSearchOutputDTO> mapOFSupplierTODunsData;
	ZyrichInvoked zy;
	Entry<String, TenantSearchDTO> suppLierMap;
	String LEUCENE_REPO_NAME;
	CountDownLatch latch;
	private static ISOAlphaNumericGenerator isoAlphaNumericGenerator;
	
	public SupplierSearchTask(Map<String, SupplierSearchOutputDTO> mapOFSupplierTODunsData,ZyrichInvoked zy,Entry<String, TenantSearchDTO> suppLierMap,String LEUCENE_REPO_NAME,CountDownLatch latch)
	{
		
		this.mapOFSupplierTODunsData=mapOFSupplierTODunsData;
		this.zy=zy;
		this.suppLierMap=suppLierMap;
		this.LEUCENE_REPO_NAME=LEUCENE_REPO_NAME;
		this.latch=latch;
	}
	
	private static final String ACCOUNT_NAME = "accountName";
	private static final String SUPPLIER_COUNTRY = "country";
	private static final String SUPPLIER_CITY = "city";
	private static final String SUPPLIER_STATE = "state";
	private static final String SUPPLIER_ZIPCODE = "zipCode";
	private static final Log logger = LogFactory.getLog(SupplierSearchTask.class);

	public void run()
	{
		try {
			SupplierSearchOutputDTO supplierSearchOutputDTO = new SupplierSearchOutputDTO();
			Map<String, String> mapOFValues = null;
			try {

				Map<String, String> context_param = processInput(suppLierMap.getValue());

				mapOFValues = zy.runJobInTOS(context_param);
				if (null != mapOFValues && !mapOFValues.isEmpty()) {

					if (mapOFValues.get("DUNS_NUM") != null) {

						mapOFValues.put("DUNS_NUM",
								Base64.getEncoder().encodeToString(mapOFValues.get("DUNS_NUM").getBytes("utf-8")));

					}
					supplierSearchOutputDTO.setRequiredInfo(mapOFValues);
					supplierSearchOutputDTO
							.setAccountName(suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME));
					supplierSearchOutputDTO.setLocation(getLocationDTO(suppLierMap.getValue()));
					mapOFSupplierTODunsData.put(
							suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
									+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY),
							supplierSearchOutputDTO);
				} else {
					mapOFSupplierTODunsData.put(suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
							+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY), null);
					logger.info("Error NO data found for  : "
							+ suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
							+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY));
				}
			} catch (Exception e) {
				mapOFSupplierTODunsData.put(suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
						+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY), null);
				logger.info("Error NO data found with error for  : "
						+ suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
						+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY));
				return;
			}
		}
		catch (Throwable e)
		{
			logger.error("Excption in processing of supplier search task",e);
		}
		finally
		{
			latch.countDown();
		}

	}
	
	private LocationDTO getLocationDTO(TenantSearchDTO tenantSearchDTO) {
		LocationDTO location=new LocationDTO();
		location.setCountry(tenantSearchDTO.getSearchParams().get(SUPPLIER_COUNTRY));
		location.setCity(tenantSearchDTO.getSearchParams().get(SUPPLIER_CITY));
		location.setState(tenantSearchDTO.getSearchParams().get(SUPPLIER_STATE));
		location.setZipCode(tenantSearchDTO.getSearchParams().get(SUPPLIER_ZIPCODE));
		return location;
	}
	
	private Map<String, String> processInput(TenantSearchDTO tenantSearchDTO) throws IOException {
		Map<String, String> context_param=new HashMap<String, String>();
		isoAlphaNumericGenerator=new ISOAlphaNumericGenerator();

	
		context_param.put("ZY_FIELDS", StringUtils.join(tenantSearchDTO.getRequiredInfo(), ","));
		context_param.put("ZY_REPOSITORY_NAME", LEUCENE_REPO_NAME);  //put pro file
		context_param.put("ZY_TAGS", ""); // may be reqd
		context_param.put("projectName", "Merlin");
		String suppName=null;
		String country=null;
		String state=null;
		String city=null;
		String zipCode=null;
		
		for(Entry<String, String> map:tenantSearchDTO.getSearchParams().entrySet()) {
			if(StringUtils.isNotBlank(map.getValue())) {
				if(map.getKey().equalsIgnoreCase("accountName")) {
					suppName=map.getValue();
					suppName=suppName.toLowerCase();				
					//CLEANING LOGIC
					//FOR SOME SUPPLIERS ENCLOSED IN DOUBLE QUOTES
//					suppName=suppName.replace("\"", "");
					suppName=InvokeCleaning.cleanVendorName(suppName);
				}else if(map.getKey().equalsIgnoreCase("country")) {
					country=map.getValue();
					country= isoAlphaNumericGenerator.getISOAlphaNumericCode(country);
					country=country.toLowerCase();
				}else if(map.getKey().equalsIgnoreCase("state")) {
					state=map.getValue();
					state = SpecialCharacterCleansing.replaceChar(state);

					if (country != null && country != "" && state != "" && state.length() > 0) {
						removeRegionCountry(state, country, null);
					}

					if (state == null || state == "") {
						state = "null";
					} else {
						state = isoAlphaNumericGenerator.getStateCode(state);
					}
					state=state.toLowerCase();
//					regionTemp = state;
				}else if(map.getKey().equalsIgnoreCase("city")) {
					city=map.getValue();
					city = SpecialCharacterCleansing.replaceChar(city);
					if (city != "") {
						removeRegionCountry(city, country, state);
					}

					if (city == null || city == ""  || city.trim().length() == 0) {
						city = "null";
					}
					city=city.toLowerCase();
				}else if(map.getKey().equalsIgnoreCase("zipCode")) {
					zipCode=map.getValue();
				}
			}
		}
		if(StringUtils.isBlank(suppName)) {
			throw new RuntimeException("Account name is mandatory");
		}
		context_param.put("ZY_COLUMN_MAPPINGS", "Vendor Name:"+suppName+",Country:"+country+",Region:"+state+",City:"+city+",Zipcode:"+zipCode);
		
	return context_param;
}

	protected String removeRegionCountry(String arrValue, String countryTemp, String regionTemp) {
		if (arrValue.trim().length() != 0) {
			String[] arrvalueSplit = arrValue.split(" ");
			for (int arrayCounter = 0; arrayCounter < arrvalueSplit.length; arrayCounter++) {
				if (countryTemp != null && countryTemp != "" && countryTemp.equalsIgnoreCase(
						isoAlphaNumericGenerator.getISOAlphaNumericCodeChk(arrvalueSplit[arrayCounter]))) {
					arrValue = arrValue.replaceAll(arrvalueSplit[arrayCounter], "");
				}

				if (regionTemp != null && regionTemp != "" && regionTemp
						.equalsIgnoreCase(isoAlphaNumericGenerator.getStateCodeChk(arrvalueSplit[arrayCounter]))) {
					arrValue = arrValue.replaceAll(arrvalueSplit[arrayCounter], "");
				}
			}
		}

		return arrValue;
	}

}
