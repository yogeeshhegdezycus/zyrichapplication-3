package com.supplierrisk.supplierrisk.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.context.annotation.ComponentScan;

import com.supplierrisk.supplierrisk.controller.SupplierRiskZyrichController;
/**
 * 
 * @author joydeep.chakraborty
 *
 */
@SpringBootApplication
@ComponentScan(basePackageClasses = SupplierRiskZyrichController.class)
public class SupplierRiskApplication {

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(SupplierRiskApplication.class);
		// SpringApplication.run(SupplierRiskApplication.class, args);
		springApplication.addListeners(new ApplicationPidFileWriter("./bin/app.pid"));
		springApplication.run(args);
	}

}
